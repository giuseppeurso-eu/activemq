package eu.giuseppeurso.activemq.kafkabasic;


import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;


public class GUKafkaConsumer {


	private static Logger _log = LogManager.getLogger(GUKafkaConsumer.class);


	/**
	 * Minimal KafkaConsumer
	 * @param kafkaServer
	 * @param topicName
	 * @return
	 */
	public static KafkaConsumer<String, String> createConsumer(String kafkaServer, String topicName) {
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "MDM");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		// Create the consumer using props.
		final KafkaConsumer<String, String> consumer =	new KafkaConsumer<>(props);

		// Subscribe to the topic.
		consumer.subscribe(Collections.singletonList(topicName));
		return consumer;
	}

	/**
	 * Kafka Connection checker
	 * @param kafkaServer
	 * @param checkTimeoutMillsec
	 * @return
	 */
	public static Boolean checkKafkaServer(String kafkaServer, int checkTimeoutMillsec) {
		Properties props = new Properties();
		props.put("bootstrap.servers", kafkaServer);

		// IMPORTANT NOTE: default.api.timeout.ms  >=  request.timeout.ms
		//
		props.put("default.api.timeout.ms", checkTimeoutMillsec);// default 60000
		props.put("request.timeout.ms", checkTimeoutMillsec);// default 30000
		AdminClient kafkaClient = AdminClient.create(props);

		Boolean isReachable = false;
		java.util.Date now = new java.util.Date();
		try {
			System.out.println("Checking Kafka Server ("+kafkaServer+")..."+now);
			Collection<Node> nodes = AdminClient.create(props).describeCluster().nodes().get();
			now = new java.util.Date();
			System.out.println("Check finished..."+kafkaServer+": "+now);
			isReachable = nodes != null && nodes.size() > 0;
		} catch (InterruptedException e) {
			now = new java.util.Date();
			System.out.println("Kafka Connection Error: "+now);
			e.printStackTrace();
		} catch (ExecutionException e) {
			now = new java.util.Date();
			System.out.println("Kafka Connection Error: "+now);
			e.printStackTrace();
		}finally {
			kafkaClient.close(Duration.ofSeconds(10));
		}
		return isReachable;
	}


}
