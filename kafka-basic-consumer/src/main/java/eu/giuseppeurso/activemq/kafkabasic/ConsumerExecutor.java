package eu.giuseppeurso.activemq.kafkabasic;


import java.io.File;
import java.io.InputStream;
import java.time.Duration;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.JSONObject;


public class ConsumerExecutor {

    private static String KAFKA_SERVER;
    private static String KAFKA_SERVER_CHECK_TIMEOUT;
    private static String TOPIC_NAME;
    private static String POLLING_INTERVAL;


    /**
     * The standard main method which is used by JVM to start execution of the Java class.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // Uncomment this if you run class on your IDE
        InputStream conf = FileUtils.openInputStream(new File("src/main/resources/configuration.properties"));
//		InputStream conf = FileUtils.openInputStream(new File("configuration.properties"));
        ResourceBundle settings = new PropertyResourceBundle(conf);

        KAFKA_SERVER = settings.getString("kafka.server.url");
        TOPIC_NAME = settings.getString("kafka.server.topic");
        KAFKA_SERVER_CHECK_TIMEOUT = settings.getString("kafka.server.check.timeoutMillsec");
        POLLING_INTERVAL = settings.getString("kafka.server.consumer.pollingIntervalMillsec");

        if(GUKafkaConsumer.checkKafkaServer(KAFKA_SERVER, Integer.parseInt(KAFKA_SERVER_CHECK_TIMEOUT))){
            System.out.println("Successfully connected to Kafka Server: "+KAFKA_SERVER);
            runConsumer();
        }else {
            System.out.println("Kafka Server is unreachable..."+KAFKA_SERVER);
        }
    }


    public static void runConsumer(){

        System.out.println("Initializing Consumer on..... "+KAFKA_SERVER+" > Topic="+TOPIC_NAME);

        try (KafkaConsumer consumer = GUKafkaConsumer.createConsumer(KAFKA_SERVER,TOPIC_NAME)) {
            while (true) {
                Date d = new Date();
                System.out.println(d+" - Polling for new messages...");
                long interval = Long.parseLong(POLLING_INTERVAL);
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(interval));//timeout
                for (ConsumerRecord<String, String> record : records) {
                    System.out.println(d+" - Message received >>>>> Topic="+record.topic()+
                            ", Partition="+record.partition()+
                            ", Offset="+record.offset() +
                            ", Key="+record.key()+
                            ", Value="+record.value());
                    String msg = record.value();
                    try {
                        JSONObject msgJson = new JSONObject(msg);
                        System.out.println("Msg ID:  "+msgJson.getString("id"));
                        System.out.println("Source:  "+msgJson.getString("source"));
                    }catch (Exception ex){
                        System.out.println("Unable to create JSON object from message value.");
                        ex.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();


        }
    }


}