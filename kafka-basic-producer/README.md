#### Kafka commands
```
## Start Kafka server (with Zookeper)
<inst_dir>/bin/zookeeper-server-start.sh   /home/giuseppe/apps/kafka/kafka_2.13-3.3.1/config/zookeeper.properties
<inst_dir>/bin/kafka-server-start.sh /home/giuseppe/apps/kafka/kafka_2.13-3.3.1/config/server.properties

## List Topics
<inst_dir>/bin/kafka-topics.sh --list --bootstrap-server localhost:9092

## Topic Details
 <inst_dir>/bin/kafka-topics.sh --describe --topic test-topic  --bootstrap-server localhost:9092
```

#### Kafka Producer
```
## Run Producer
mvn clean install
cd target
java -jar kafka-producer.jar
```