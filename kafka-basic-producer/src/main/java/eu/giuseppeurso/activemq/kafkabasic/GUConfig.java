package eu.giuseppeurso.activemq.kafkabasic;

import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class GUConfig {

    static InputStream conf =  GUConfig.class.getResourceAsStream("/configuration.properties");
    static ResourceBundle settings;

    static String KAFKA_SERVER;

    static int KAFKA_REQUEST_TIMEOUT;
    static int KAFKA_API_TIMEOUT;

    static String TOPIC_NAME;
    static int MSG_TO_SEND;


    /**
     * Properties initializer
     */
    static{
        if(settings==null){
            try {
                settings  = new PropertyResourceBundle(conf);
            } catch (IOException e) {
                System.err.println("Unable to initialize Resource Bundle: "+conf);
                e.printStackTrace();
            }
        }

        KAFKA_SERVER = settings.getString("kafka.server.url");
        KAFKA_REQUEST_TIMEOUT = Integer.parseInt(settings.getString("kafka.server.request.timeoutMillsec"));
        KAFKA_API_TIMEOUT = Integer.parseInt(settings.getString("kafka.server.api.timeoutMillsec"));
        TOPIC_NAME = settings.getString("kafka.server.topic");
        MSG_TO_SEND = Integer.parseInt(settings.getString("kafka.msg.toSend"));

    }


}
