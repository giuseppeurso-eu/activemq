package eu.giuseppeurso.activemq.kafkabasic;


import java.io.IOException;


public class Executor {

    /**
     * The standard main method which is used by JVM to start execution of Java classes.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws IOException {


        if(GUKafkaProducer.checkKafkaServer()){
            System.out.println("Successfully connected to Kafka Server: "+ GUConfig.KAFKA_SERVER);
            GUKafkaProducer.runProducer(GUConfig.MSG_TO_SEND);
        }else {
            System.out.println("Kafka Server is unreachable..."+ GUConfig.KAFKA_SERVER);
        }
    }









}