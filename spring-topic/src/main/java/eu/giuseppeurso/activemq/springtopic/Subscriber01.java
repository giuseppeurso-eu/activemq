package eu.giuseppeurso.activemq.springtopic;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQMapMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.log4j.Logger;

/**
 * 
 * @author cira
 *
 */
public class Subscriber01 implements MessageListener {

	private static Logger _log = Logger.getLogger(Subscriber01.class);
	
    public void onMessage(Message message)
    {
        try
        {
            if (message instanceof TextMessage) {
            	ActiveMQTextMessage amqMessage = (ActiveMQTextMessage) message;
            	_log.debug("(SUBSCRIBER-01) Message received >> " + amqMessage.getText());
                try {
					Thread.sleep((long)(1000+Math.random()*4000));
				} catch (InterruptedException e) {	}
                _log.debug("Message map processed >> " + message.getJMSMessageID());
            } else {
            	_log.debug("Invalid message received");
            }
        }
        catch (JMSException e)
        {
            System.out.println("Caught:" + e);
            e.printStackTrace();
        }
    }

    
}
