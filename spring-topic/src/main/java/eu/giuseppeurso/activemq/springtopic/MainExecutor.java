package eu.giuseppeurso.activemq.springtopic;

import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class MainExecutor 
{
	public static void main(String[] args) throws Exception {
        FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("src/main/resources/SpringBeans.xml");
        Publisher producer = (Publisher)context.getBean("publisher");
        Session session = producer.getTemplate().getConnectionFactory().createConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
		TextMessage txtMessage = session.createTextMessage();
		txtMessage.setText("Messaggio di test");
        
        producer.sendMessage(txtMessage);
//        producer.start();
	}
}