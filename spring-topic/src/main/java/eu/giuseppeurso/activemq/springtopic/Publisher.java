package eu.giuseppeurso.activemq.springtopic;

import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class Publisher {

	private static Logger _log = Logger.getLogger(Publisher.class);
	private static JmsTemplate template;
	
	/**
	 * A default constructor.
	 */
	public Publisher() {
    }    

	/**
	 * This method uses the JmsTemplate's convertAndSend() to send messages.
	 * @param message
	 * @param queueName
	 * @throws JMSException
	 */
	public static void sendMessage(Message message) throws JMSException {
		Destination defaultDestination = template.getDefaultDestination();
		template.convertAndSend(defaultDestination, message);
        _log.debug("Published message "+message.getJMSMessageID()+" on queue: "+defaultDestination);
    }	
	
	/**
	 * Basic setter injection for property "JmsTemplate" using Spring Beans definition. See the Spring XML
	 * application context file. 
	 * @param template
	 */
	public void setTemplate(JmsTemplate template) {
		this.template = template;
	}

	/**
	 * JmsTemplate getter.
	 * @return the template
	 */
	public static JmsTemplate getTemplate() {
		return template;
	}
	
}