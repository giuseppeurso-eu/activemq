package eu.giuseppeurso.activemq.kafkassl;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.JSONObject;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Basic Kafka message producer
 */
public class GUKafkaProducer {

    public static Properties initKafkaProperties(){
        Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, GUConfig.KAFKA_SERVER);
        //props.put(ProducerConfig.CLIENT_ID_CONFIG, "ID-GU-client");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // IMPORTANT NOTE: Must be default.api.timeout.ms >=  request.timeout.ms
        //
//      props.put("default.api.timeout.ms", checkTimeout);//default 60000
//      props.put("request.timeout.ms", checkTimeout);// default 30000
        props.put(ConsumerConfig.DEFAULT_API_TIMEOUT_MS_CONFIG, GUConfig.KAFKA_API_TIMEOUT);
        props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, GUConfig.KAFKA_REQUEST_TIMEOUT);
        props.put(ProducerConfig.METADATA_MAX_IDLE_CONFIG, 5000);
//      props.put(ProducerConfig.MET)
//      properties.put("metadata.fetch.timeout.ms", 1000);

        if(GUConfig.KAFKA_SSL_ENABLED){
            System.out.println("SSL ENABLED");
            if(!GUConfig.KAFKA_SSL_HOSTNAME_VERIFICATION_ENABLED){
                props.put(SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG,"");
            }
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
            props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, GUConfig.KAFKA_SSL_KEYSTORE_LOCATION);
            props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, GUConfig.KAFKA_SSL_KEYSTORE_PASS);
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, GUConfig.KAFKA_SSL_TRUSTSTORE_LOCATION);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, GUConfig.KAFKA_SSL_TRUSTSTORE_PASS);
            System.out.println("Loaded Keystore: "+ GUConfig.KAFKA_SSL_KEYSTORE_LOCATION);
            System.out.println("Loaded Truststore: "+ GUConfig.KAFKA_SSL_TRUSTSTORE_LOCATION);

        }
        return props;
    }

    /**
     * Return a minimal KafkaProducer object
     * @param kafkaServer
     * @return
     */
    public static KafkaProducer<String, String> createProducer(String kafkaServer) {
        Properties props = initKafkaProperties();
        return new KafkaProducer<>(props);
    }


    /**
     * Date object to ISO8601 date format string
     * @param srcDate
     * @return
     */
    public static String toISO8601(Date srcDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        return sdf.format(srcDate);
    }

    /**
     * Creates the Kafka Producer and sends messages to the server
     * @param msgToSend
     */
    public static void runProducer(int msgToSend){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("Initializing Producer for..... "+ GUConfig.KAFKA_SERVER+" > Topic="+ GUConfig.TOPIC_NAME);
        try (KafkaProducer<String, String> producer =  GUKafkaProducer.createProducer(GUConfig.KAFKA_SERVER)) {
            int counter = 1;

            System.out.println("Start Producer: "+ sdf.format(new Date()));

            while (counter <= msgToSend) {
                System.out.println("Sending msg..."+counter);
                //String msg = "GU Message " + counter;

                String msg = GUKafkaProducer.createJsonMessage().toString();

                //Use key if you want all the messages to go to a single partition
                ProducerRecord<String, String> message = new ProducerRecord<>(GUConfig.TOPIC_NAME, msg);
                RecordMetadata meta = producer.send(message).get();
                System.out.println("Message sent >>>> Topic="+ GUConfig.TOPIC_NAME+", Partition="+meta.partition()+ ", Message="+msg);
                counter++;
            }
            producer.close(Duration.ofSeconds(5));
            System.out.println("Close Producer: "+sdf.format(new Date()));
        } catch (Exception e) {
            System.out.println("Producer ERROR - "+sdf.format(new Date()));
            e.printStackTrace();
        }finally {

        }
    }

    /**
     * Kafka Server connection checker
     * @return
     */
    public static Boolean checkKafkaServer() {
        Properties props = initKafkaProperties();

        AdminClient kafkaClient = AdminClient.create(props);

        Boolean isReachable = false;
        java.util.Date now = new java.util.Date();
        try {
            System.out.println("Checking Kafka Server ("+ GUConfig.KAFKA_SERVER+")..."+now);
            Collection<Node> nodes = kafkaClient.describeCluster().nodes().get();
            now = new java.util.Date();
            System.out.println("Check finished..."+ GUConfig.KAFKA_SERVER+": "+now);
            isReachable = nodes != null && nodes.size() > 0;
        } catch (InterruptedException e) {
            now = new java.util.Date();
            System.out.println("Kafka Connection Error: "+now);
            e.printStackTrace();
        } catch (ExecutionException e) {
            now = new java.util.Date();
            System.out.println("Kafka Connection Error: "+now);
            e.printStackTrace();
        }finally {
            kafkaClient.close(Duration.ofSeconds(10));
        }
        return isReachable;
    }

    /**
     * Message Template 1
     * @return
     */
    public static JSONObject createJsonMessage(){
        java.util.Date now = new java.util.Date();
        JSONObject msgJson = new JSONObject();
        msgJson.put("specversion","1.0");
        msgJson.put("id","21627e26-31eb-43e7-8343-92a696fd96b1_"+now.getTime());
        msgJson.put("source","TEST");
        msgJson.put("time",toISO8601_2(now));
        return msgJson;
    }



    /**
     * Date object to ISO8601 date format string
     * @param srcDate
     * @return
     */
    public static String toISO8601_2(java.util.Date srcDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSXXX");
        return sdf.format(srcDate);
    }







}
