package eu.giuseppeurso.activemq.kafkassl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class GUConfig {

    static InputStream conf =  GUConfig.class.getResourceAsStream("/configuration.properties");
    static ResourceBundle settings;

    static String KAFKA_SERVER;

    static int KAFKA_REQUEST_TIMEOUT;
    static int KAFKA_API_TIMEOUT;

    static Boolean KAFKA_SSL_ENABLED;
    static Boolean KAFKA_SSL_HOSTNAME_VERIFICATION_ENABLED;
    static String KAFKA_SSL_KEYSTORE_LOCATION;
    static String KAFKA_SSL_KEYSTORE_PASS;
    static String KAFKA_SSL_TRUSTSTORE_LOCATION;
    static String KAFKA_SSL_TRUSTSTORE_PASS;

    static String TOPIC_NAME;
    static int MSG_TO_SEND;


    /**
     * Properties initializer
     */
    static{
        if(settings==null){
            try {
                settings  = new PropertyResourceBundle(conf);
            } catch (IOException e) {
                System.err.println("Unable to initialize Resource Bundle: "+conf);
                e.printStackTrace();
            }
        }

        KAFKA_SERVER = settings.getString("kafka.server.url");
        KAFKA_REQUEST_TIMEOUT = Integer.parseInt(settings.getString("kafka.server.request.timeoutMillsec"));
        KAFKA_API_TIMEOUT = Integer.parseInt(settings.getString("kafka.server.api.timeoutMillsec"));
        TOPIC_NAME = settings.getString("kafka.server.topic");
        MSG_TO_SEND = Integer.parseInt(settings.getString("kafka.msg.toSend"));

        KAFKA_SSL_ENABLED = Boolean.parseBoolean(settings.getString("kafka.ssl.enabled"));
        KAFKA_SSL_HOSTNAME_VERIFICATION_ENABLED = Boolean.parseBoolean(settings.getString("kafka.ssl.hostNameVerification.enabled").trim());

        String keystorePath = settings.getString("kafka.ssl.keystore.location").trim();
        try {
            File keystoreFile = loadFile(keystorePath);
            KAFKA_SSL_KEYSTORE_LOCATION = keystoreFile.getAbsolutePath();
            KAFKA_SSL_KEYSTORE_PASS = settings.getString("kafka.ssl.keystore.password");
        } catch (Exception e) {
            e.printStackTrace();
        }

        String truststorePath = settings.getString("kafka.ssl.truststore.location").trim();
        try {
            File truststoreFile = loadFile(truststorePath);
            KAFKA_SSL_TRUSTSTORE_LOCATION = truststoreFile.getAbsolutePath();
            KAFKA_SSL_TRUSTSTORE_PASS = settings.getString("kafka.ssl.truststore.password");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static File loadFile(String filePath) throws Exception {
        File f = null;
        // Load via system path
        try {
            f = new File(filePath);
            if (fileExists(f)){
                System.out.println("Loading file (via system path): "+filePath);
                return f;
            }
        } catch (Exception e) {
            System.out.println("Errore caricamento file via system path: "+filePath+" -  Tentativo caricamento via classpath...");
        }
        // Load via classpath
        try {
            URL resource = GUConfig.class.getResource(filePath);
            f = new File(resource.toURI());
        } catch (Exception ex) {
            System.out.println("Errore caricamento file via classpath: "+filePath);
        }
        if (fileExists(f)){
            System.out.println("Loading file (via classpath): "+filePath);
            return f;
        }else{
            System.out.println("Impossibile caricare il file: "+filePath);
            throw new Exception();
        }
    }


    /**
     *
     * @param f
     * @return
     */
    public static Boolean fileExists(File f){
        if(f.exists() && !f.isDirectory()){
            return true;
        }else{
            return false;
        }
    }
}
