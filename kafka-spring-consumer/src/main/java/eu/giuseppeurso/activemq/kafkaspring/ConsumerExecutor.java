package eu.giuseppeurso.activemq.kafkaspring;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;


@SpringBootApplication
@EnableKafka
public class ConsumerExecutor {

    /**
     * The standard main method used to run this application by Springboot.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) {
        SpringApplication.run(ConsumerExecutor.class, args);

    }



}