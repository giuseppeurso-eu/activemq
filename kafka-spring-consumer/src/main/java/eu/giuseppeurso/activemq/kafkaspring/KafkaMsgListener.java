package eu.giuseppeurso.activemq.kafkaspring;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaMsgListener {


    @KafkaListener(topics = "${kafka.server.topic:#{test-topic}}", groupId = "${kafka.groupId:#{test-id}}")
    public void listener(String message) {
        System.out.println("Received Message: " + message);
    }

}
