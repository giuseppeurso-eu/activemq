package eu.giuseppeurso.activemq.camel.ftp.producer;

import java.util.Map;

import eu.giuseppeurso.activemq.camel.ftp.producer.*;
import junit.framework.TestCase;

/**
 * Test cases 
 * 
 * @author Giuseppe Urso
 * 
 */
public class ConsumerRouteBuilderTest extends TestCase {

	private static String testResourceDir;
	private static String testFile01;
	
	@Override
	protected void setUp() throws Exception {
		testResourceDir = "src/test/resources";	
		testFile01 = testResourceDir + "/test.txt";
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
		
	/**
	 * Test 
	 * 
	 */
	public void testGetPropertiesMap() {
		boolean assertActual = false;
		
		if (FtpRouteBuilder.getPropertiesMap()!=null && FtpRouteBuilder.getPropertiesMap().size()>0) {
			assertActual=true;
			for (Map.Entry<String, String> entry : FtpRouteBuilder.getPropertiesMap().entrySet())
			{
			    System.out.println("TEST [" + this.getName()+"] "+entry.getKey() + ": " + entry.getValue());
			}
		}
		assertEquals("TEST FAILURE " + this.getName(), true, assertActual);	
	}
}
