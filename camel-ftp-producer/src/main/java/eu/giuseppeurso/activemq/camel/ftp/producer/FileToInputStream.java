package eu.giuseppeurso.activemq.camel.ftp.producer;

import java.io.File;
import java.io.IOException;


import org.apache.commons.io.FileUtils;

public class FileToInputStream {
	

	public byte[] getBytes() throws IOException {
		
		File f = new File("src/test/resources/test.txt");
		byte[] fileByte = FileUtils.readFileToByteArray(f);
		return fileByte;
	}
}
